# LxyTool

[![CI Status](https://img.shields.io/travis/lxyWirhRongMrs/LxyTool.svg?style=flat)](https://travis-ci.org/lxyWirhRongMrs/LxyTool)
[![Version](https://img.shields.io/cocoapods/v/LxyTool.svg?style=flat)](https://cocoapods.org/pods/LxyTool)
[![License](https://img.shields.io/cocoapods/l/LxyTool.svg?style=flat)](https://cocoapods.org/pods/LxyTool)
[![Platform](https://img.shields.io/cocoapods/p/LxyTool.svg?style=flat)](https://cocoapods.org/pods/LxyTool)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LxyTool is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LXYToolProject'
```

## Author

lxyWirhRongMrs, 1556937213@qq.com

## License

LxyTool is available under the MIT license. See the LICENSE file for more info.
