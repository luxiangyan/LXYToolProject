//
//  LXYTool.swift
//  LxyTool
//
//  Created by GY on 2018/12/4.
//

import Foundation

//MARK: - 添加左右元素项
public func MCBarButtonItem_image(navItem : UINavigationItem, size : CGSize, target : Any, selector : Selector, image : String, isLeft : Bool)  {
    let button = UIButton.init(type: UIButtonType.custom)
    button.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    button.setBackgroundImage(UIImage.init(named: image), for: UIControlState.normal)
    button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
    button.adjustsImageWhenHighlighted = false
    
    if isLeft {
        navItem.leftBarButtonItem = UIBarButtonItem.init(customView: button)
    } else {
        navItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
    }
}
public func MCBarButtonItem_text(navItem : UINavigationItem, size : CGSize, target : Any, selector : Selector, text : String, textColor : UIColor, font : CGFloat, isLeft : Bool)  {
    let button = UIButton.init(type: UIButtonType.custom)
    button.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
    button.adjustsImageWhenHighlighted = false
    
    button.titleLabel?.font = UIFont.systemFont(ofSize: font)
    button.setTitle(text, for: UIControlState.normal)
    button.setTitleColor(textColor, for: UIControlState.normal)
    
    if isLeft {
        navItem.leftBarButtonItem = UIBarButtonItem.init(customView: button)
    } else {
        navItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
    }
}


//MARK: -MBProgressHUD
//public func MCHUD_default(message : String?,delay : CGFloat,view : UIView?) {
//    var text = message
//    if text == nil { text = "加载中"}
//
//    var superView = view
//    if view == nil { superView = getView() }
//
//    let hud = MBProgressHUD.showAdded(to: superView!, animated: true)
//    hud.label.text = text
//
//    if delay != 0 { hud.hide(animated: true, afterDelay: TimeInterval(delay)) }
//}
//func MCHUD_text(text : String,view : UIView?,delay : Double) {
//
//    var superView = view
//    if view == nil { superView = getView() }
//
//    let hud = MBProgressHUD.showAdded(to: superView!, animated: true)
//    hud.label.text = text
//    hud.mode = MBProgressHUDMode.text
//    hud.hide(animated: true, afterDelay: delay)
//}
//func MCHUD_custom(text : String,view : UIView?,delay : Double,image: String) {
//
//    var superView = view
//    if view == nil { superView = getView() }
//
//    let hud = MBProgressHUD.showAdded(to: superView!, animated: true)
//    hud.label.text = text
//    hud.mode = MBProgressHUDMode.customView
//    hud.customView = UIImageView.init(image: UIImage.init(named: image))
//
//    hud.hide(animated: true, afterDelay: delay)
//}
//
//func MCHUD_allFinishedLoading(view : UIView?) {
//
//    var superView = view
//    if view == nil { superView = getView() }
//
//    let hud = MBProgressHUD.showAdded(to: superView!, animated: true)
//    hud.label.text = "到底了啦"
//    hud.mode = MBProgressHUDMode.text
//    hud.offset = CGPoint.init(x: 0, y: (view?.frame.size.height)! / 2 - 50)
//    hud.hide(animated: true, afterDelay: 1.0)
//}
//
//
//public func MCHUD_hidden(view : UIView?) {
//    var superView = view
//    if view == nil { superView = getView() }
//    MBProgressHUD.hide(for: superView!, animated: true)
//}
private func getView() -> UIView {
    return UIApplication.shared.keyWindow!
}


typealias MCAlterConfirm = () -> Void
//MARK: -UIAlertController
func MCAlterController_twoOptions(vc : UIViewController,message:String,confirm:@escaping MCAlterConfirm) {
    
    let alter = UIAlertController.init(title: message, message: "", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancle = UIAlertAction.init(title: "取消", style: UIAlertActionStyle.cancel) { (a) in
    }
    
    let confirm = UIAlertAction.init(title: "确定", style: UIAlertActionStyle.default) { (b) in
        confirm()
    }
    
    alter.addAction(cancle)
    alter.addAction(confirm)
    vc.present(alter, animated: true, completion: nil)
}

func MCAlterController_oneOptions(vc : UIViewController,message:String) {
    
    let alter = UIAlertController.init(title: message, message: "", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancle = UIAlertAction.init(title: "取消", style: UIAlertActionStyle.cancel) { (a) in
    }
    alter.addAction(cancle)
    vc.present(alter, animated: true, completion: nil)
}

//func MCAlterController_oneOptions(vc : UIViewController,message:String,confirm:@escaping AlterConfirm) {
//
//    let alter = UIAlertController.init(title: message, message: "", preferredStyle: UIAlertControllerStyle.alert)
//
//    let cancle = UIAlertAction.init(title: "取消", style: UIAlertActionStyle.cancel) { (a) in
//        confirm()
//    }
//    alter.addAction(cancle)
//    vc.present(alter, animated: true, completion: nil)
//}



func MCAlterController_abnormalNetwork(vc : UIViewController) {
    
    let alter = UIAlertController.init(title: "Sorry,网络开了个小差", message: "", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancle = UIAlertAction.init(title: "取消", style: UIAlertActionStyle.cancel) { (a) in
    }
    alter.addAction(cancle)
    vc.present(alter, animated: true, completion: nil)
}


public func MCPushToViewController(_ currentVC : UIViewController,_ targetVC : UIViewController,_ isHidesBottomBarWhenBack : Bool) {
    
    weak var weakSelf = currentVC
    
    weakSelf?.hidesBottomBarWhenPushed = true
    weakSelf?.navigationController?.pushViewController(targetVC, animated: true)
    weakSelf?.hidesBottomBarWhenPushed = isHidesBottomBarWhenBack
}

